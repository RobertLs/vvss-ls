package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class CartiRepoTest {
    public CartiRepoMock repo;
    List<Carte> carti;
    List<String> referenti = new ArrayList<String>();
    List<String> cuvinteCheie = new ArrayList<String>();
    @Before
    public void setUp() throws Exception {
       repo = new CartiRepoMock();
       carti  = new ArrayList<Carte>();
        List<String> referenti = new ArrayList<String>();
        List<String> cuvinteCheie = new ArrayList<String>();
    }

    @Test
    public void cautaCarteNull() {
        carti = repo.cautaCarte("Badea");
        assertEquals(carti.size(),0);
    }

    @Test
    public void cautaCarteNenull(){
        carti = repo.cautaCarte("Mihai Eminescu");
        assertEquals(carti.size(),1);
    }

//    @Test
//    public void




























    @Test
    public void adaugaCarte(){
        Carte c = new Carte("12",referenti,"1997",cuvinteCheie);
        repo.adaugaCarte(c);
        assertEquals(carti.size(),0);
    }

    @Test
    public void Valid(){
        Carte c = new Carte("Nume",referenti,"1997",cuvinteCheie);
        repo.adaugaCarte(c);
        assertEquals(carti.size(),0);
    }
}