package biblioteca.repository.repoMock;

import biblioteca.model.Carte;
import com.sun.media.sound.InvalidFormatException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {
    CartiRepoMock cartiRepo;
    List<String> referenti = new ArrayList<String>();
    List<String> cuvinteCheie = new ArrayList<String>();
    int sizeBefore;

    @Before
    public void setUp() throws Exception {
        cartiRepo = new CartiRepoMock();
        sizeBefore = cartiRepo.getCarti().size();


    }

    @After
    public void tearDown() throws Exception {

    }

    //ECP Valid
    @Test
    public void addCarteECP_Valid() throws Exception{

        Carte c = new Carte("Nume",referenti,"1997",cuvinteCheie);
        cartiRepo.adaugaCarte(c);

        assertEquals(sizeBefore + 1,cartiRepo.getCarti().size());

    }

    //ECP Valiv
    @Test
    public void addCarteECP_Valid2() throws  Exception{
        Carte  c2 = new Carte("Nume2",referenti,"137",cuvinteCheie);
        cartiRepo.adaugaCarte(c2);

        assertEquals(sizeBefore ++ ,cartiRepo.getCarti().size());
    }
    //ECP Nevalid
    @Test
    public void addCarteECP_Nevalid() throws Exception{

        try{
            Carte c = new Carte("123",referenti,"1997",cuvinteCheie);
            cartiRepo.adaugaCarte(c);
        }catch(Exception e){
           assertEquals(e.getMessage(),"Lista cuvinte cheie vida");
        }
        finally {
            assertEquals(sizeBefore  ,cartiRepo.getCarti().size());
        }

    }

  //BVA Valid
    @Test
    public void addCarteBVA_Valid() throws  Exception {
        cuvinteCheie.add("cuvant");
        cuvinteCheie.add("cuvant");

        Carte c = new Carte("Nume", referenti, "1997", cuvinteCheie);
        cartiRepo.adaugaCarte(c);

        assertEquals(++sizeBefore, cartiRepo.getCarti().size());

    }

    //BVA valid

    @Test
    public void addCarteBVA_Valid2() throws Exception{
        referenti.add("referent");

        Carte c = new Carte("Nume", referenti, "1997", cuvinteCheie);
        cartiRepo.adaugaCarte(c);


        assertEquals(++sizeBefore, cartiRepo.getCarti().size());
    }
    //BVA invalid

    @Test
    public void addCarteBVA_Invalid() throws Exception{
        cuvinteCheie = null;

        try {
            Carte c = new Carte("Nume", referenti, "1997", cuvinteCheie);
            cartiRepo.adaugaCarte(c);
        }catch(Exception e) {
            assertEquals(e.getMessage(),"Lista cuvinte cheie vida");

        }finally{
            assertEquals(sizeBefore, cartiRepo.getCarti().size());
        }
    }

    @Test
    public void addCarteBVA_Invalid2() throws Exception  {
        cuvinteCheie = null;

       try {
           Carte c = new Carte("Nume", referenti, "1997", cuvinteCheie);
           cartiRepo.adaugaCarte(c);

       }catch(Exception e) {
           assertEquals(sizeBefore, cartiRepo.getCarti().size());

       }}

}